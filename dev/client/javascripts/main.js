var CURRENT_URL = location.protocol + '//' + location.host;

var getUrlEnv = function(currentUrl) {
  try {
    return document.getElementById('vt_initializor').getAttribute('src').split('/').slice(0,-2).join('/');
  }catch(e){return currentUrl;}
};

var getNodeEnv = function(urlEnv){
  try {
    return url = document.getElementById('vt_initializor').getAttribute('data-preprocessor-url') || urlEnv;
  } catch(e) {return urlEnv;}
};

URL_ENV = getUrlEnv(CURRENT_URL);
NODE_ENV = getNodeEnv(URL_ENV);
var RECLINE_PATH = URL_ENV + '/vendor/recline/';
require.config({
  baseUrl: URL_ENV + '/javascripts',
  waitSeconds: 200,
  // paths: maps ids with paths (no extension)
  paths: {
    jquery: 'jquery',
    bootstrap: 'bootstrap/bootstrap',
    csv: RECLINE_PATH + 'vendor/csv/csv',//'http://okfnlabs.org/csv.js/csv',
    recline: 'recline',
    css: 'css',
    vt_intl: 'vt_intl',
    overlay_view: 'overlay_view',
    xlsx: 'xlsx',
    jszip: 'jszip',
    // Recline dependencies
    'backend.dataproxy': RECLINE_PATH + 'src/backend.dataproxy',
    'backend.memory': RECLINE_PATH + 'src/backend.memory',
    'ecma-fixes': RECLINE_PATH + 'src/ecma-fixes',
    model: RECLINE_PATH + 'src/model',
    'view.flot': RECLINE_PATH + 'src/view.flot',
    'view.graph': RECLINE_PATH + 'src/view.graph',
    'view.grid': RECLINE_PATH + 'src/view.grid',
    'view.map': RECLINE_PATH + 'src/view.map',
    'view.multiview': RECLINE_PATH + 'src/view.multiview',
    'view.slickgrid': RECLINE_PATH + 'src/view.slickgrid',
    'view.timeline': RECLINE_PATH + 'src/view.timeline',
    'widget.facetviewer': RECLINE_PATH + 'src/widget.facetviewer',
    'widget.fields': RECLINE_PATH + 'src/widget.fields',
    'widget.filtereditor': RECLINE_PATH + 'src/widget.filtereditor',
    'widget.pager': RECLINE_PATH + 'src/widget.pager',
    'widget.queryeditor': RECLINE_PATH + 'src/widget.queryeditor',
    'widget.valuefilter': RECLINE_PATH + 'src/widget.valuefilter',
    backbone: RECLINE_PATH + 'vendor/backbone/1.0.0/backbone',
    // FOR IE : excanvas: RECLINE_PATH + 'vendor/flot/excanvas.min',
    'jquery.flot': RECLINE_PATH + 'vendor/flot/jquery.flot',
    'jquery.flot.time': RECLINE_PATH + 'vendor/flot/jquery.flot.time',
    json: RECLINE_PATH + 'vendor/json/json2',
    leaflet: RECLINE_PATH + 'vendor/leaflet/0.7.3/leaflet',
    'leaflet.markercluster':
      RECLINE_PATH + 'vendor/leaflet.markercluster/leaflet.markercluster',
    moment: RECLINE_PATH + 'vendor/moment/2.0.0/moment',
    mustache: RECLINE_PATH + 'vendor/mustache/0.5.0-dev/mustache',
    showdown: RECLINE_PATH + 'vendor/showdown/20120615/showdown',
    'jquery-ui':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/jquery-ui-1.8.16.custom.min',
    'jquery.event.drag':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/jquery.event.drag-2.2',
    'jquery.event.drop':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/jquery.event.drop-2.2',
    'slick.core':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/slick.core',
    'slick.formatters':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/slick.formatters',
    'slick.editors':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/slick.editors',
    'slick.grid': RECLINE_PATH + 'vendor/slickgrid/2.0.1/slick.grid',
    'slick.rowselectionmodel':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/plugins/slick.rowselectionmodel',
    'slick.rowmovemanager':
      RECLINE_PATH + 'vendor/slickgrid/2.0.1/plugins/slick.rowmovemanager',
    timeline: RECLINE_PATH + 'vendor/timeline/js/timeline',
    underscore: RECLINE_PATH + 'vendor/underscore/1.4.4/underscore',
    'underscore.deferred':
      RECLINE_PATH + 'vendor/underscore.deferred/0.4.0/underscore.deferred'
  },
  // shim: makes external libraries reachable
  shim: {
    xlsx: {
      deps: ['jszip'],
      exports: 'XLSX'
    },
    'backbone': {
      'deps': ['underscore', 'jquery'],
      'exports': 'Backbone'
    },
    bootstrap: ['jquery'],
    'backend.dataproxy': ['underscore'],
    'backend.memory': ['underscore'],
    'model': ['underscore', 'backbone'],
    'view.flot': ['jquery', 'backbone'],
    'view.grid': ['jquery', 'backbone'],
    'view.multiview': ['jquery', 'backbone'],
    'view.map': ['jquery', 'backbone'],
    'widget.facetviewer': ['jquery', 'backbone'],
    'view.timeline': ['jquery', 'backbone'],
    'view.slickgrid': ['jquery', 'backbone'],
    'widget.fields': ['jquery', 'backbone'],
    'widget.pager': ['jquery', 'backbone'],
    'widget.queryeditor': ['jquery', 'backbone'],
    'widget.filtereditor': ['jquery', 'backbone'],
    'widget.valuefilter': ['jquery', 'backbone'],
    'jquery.flot': ['jquery'],
    'jquery.flot.time': ['jquery', 'jquery.flot'],
    'slick.core': ['jquery'],
    'slick.formatters': ['jquery'],
    'slick.rowselectionmodel': ['jquery'],
    'slick.rowmovemanager': ['jquery'],
    'slick.editors': ['jquery'],
    'jquery.event.drag': ['jquery'],
    'jquery.event.drop': ['jquery'],
    'jquery-ui': ['jquery'],
    'slick.grid': ['jquery-ui', 'jquery.event.drag', 'slick.core'],
    'leaflet.markercluster': ['leaflet'],
    'timeline': ['jquery'],
    'csv': ['underscore', 'recline'],
    'vt_intl': { 'exports': 'vt_intl' },
    'overlay': { 'exports': 'triggerOverlay'},
    recline: ['underscore', 'backbone', 'jquery', 'slick.grid', 'vt_intl']
  }
});

// Need to require jszip first and attach it to window
require(['jszip'], function (jszip) {
    window.JSZip = jszip;
    // internationalisation before app
    require(['xlsx', 'vt_intl'], function (XLSX, vt_intl) {
      var lang = document.getElementsByTagName('html')[0].getAttribute('lang')||'en';
      vt_intl.setLanguage(lang);
      // requier the app.js file and trigger a button click on the preview-button id element
      require([URL_ENV + '/javascripts/recline_dependencies.js', URL_ENV + '/javascripts/app.js'], function() {
         var button = document.getElementById('preview-button');
         if (button) {
           button.click();
         } else {
           alert('the preview button id needs to be "preview-button"');
         }
       });
    });
});

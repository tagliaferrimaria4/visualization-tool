define(['xlsx', 'bootstrap', 'css', 'overlay', 'csv', 'recline', 'underscore', 'overlay_view'],
  function(XLSX, bootstrap, css, overlay, csv, reclineStuff, underscore, overlay_view) {

  var rootFolder = '';
  switch (document.location.hostname)
  {
    case 'www.paneuropeandataportal.eu':
      rootFolder = '/vtool/'; break;
    case 'localhost' :
      rootFolder = ''; break;
    default:
      rootFolder = '/vtool/';
  }

  var downloadProgressCallback = function(evt) {
    if (evt.lengthComputable) {
      var percentComplete = (evt.loaded / evt.total) * 100;
      $('#progress-bar').width(percentComplete + '%');
      $('#progress-bar').text(Math.round(percentComplete) + '%');
    } else {
      $('#progress-bar').width('100%');
      $('#progress-bar').text(vt_intl.t('Loading'));
    }
  };

  var root = location.protocol + '//' + location.host;
  var OFFICE_EXT = ['doc', 'docx', 'docm', 'ppt', 'pptx', 'pptm'];

// snippetHelper
snippetHelper = {

    // create multiview
    createMultiView : function(dataset, state) {
      var $el = $('<div />');
      $el.appendTo(window.explorerDiv);

      var views = [
        {
          id: 'grid',
          label: vt_intl.t('Grid'),
          view: new recline.View.SlickGrid({
            model: dataset,
            state: {
              gridOptions: {
                // Enable support for row ReOrder
                enableReOrderRow:true,
                autoEdit: false,
                enableCellNavigation: true
              },
              columnsEditor: [
                {column: 'code', editor: Slick.Editors.Date},
                {column: 'sometext', editor: Slick.Editors.Text}
              ]
            }
          })
        },
        {
          id: 'graph',
          label: vt_intl.t('Graph'),
          view: new recline.View.Graph({
            model: dataset

          })
        }
      ];

      var multiView = new recline.View.MultiView({
        model: dataset,
        el: $el,
        state: state,
        views: views
      });

      // Add Backend Specific options to the multiview
      if (dataset.backend.__type__ == 'csv') {
        //  Add CSV options
        $('#visualization-tool .menu-right').prepend(
          '<div style="float:right; margin-left: 5px; border-left: solid 2px #ddd; padding-left: 5px">' +
            '<form class="form-inline">' +
              '<div class="form-group">' +
                '<label style="margin-right: 5px; margin-left: 10px;">' + vt_intl.t('Delimiter') + ': </label>' +
                '<select id="dataset-delimiter" class="form-control">' +
                  '<option value=",">' + vt_intl.t('Comma') + '</option>' +
                  '<option value=";">' + vt_intl.t('Semicolon') + '</option>' +
                  '<option value=" ">' + vt_intl.t('Space') + '</option>' +
                  '<option value="  ">' + vt_intl.t('Tab') + '</option>' +
                '</select>' +
              '</div>' +
              '<div class="form-group">' +
                '<label style="margin-right: 5px; margin-left: 5px;">' + vt_intl.t('Skip Rows') + ': </label>' +
                '<input type="number" value="0" min="0" id="dataset-skip-rows" class="form-control" />' +
              '</div>' +
            '</form>' +
          '</div>');
        $('#dataset-delimiter').change(function() {
          var csvDelimiter = $(this).children('option:selected').val();
          dataset.set('delimiter', csvDelimiter);
          dataset.fetch();
        });
        $('#dataset-skip-rows').change(function() {
          var rowsToSkip = $(this).val();
          dataset.set('skipInitialRows', rowsToSkip);
          dataset.fetch();
        });
      }

      return multiView;
    },

    // append sheet names to list
    appendToList: function(sheetNames) {
      var content = '<option value="'+vt_intl.t('Select a sheet')+'">'+vt_intl.t('Select a sheet')+'</option>';
      _.each(sheetNames, function(sheetName) {
        content += '<option value="' + sheetName + '">' + sheetName +
                     '</option>';
      });
      $('#sheet_names').append(content);
    },

    // generate CSV
    generateCSVDataSet: function(url, extension) {
      var dfd = jQuery.Deferred();

      extension = extension || 'csv';
      var dataset = new recline.Model.Dataset({
        url: url,
        backend: extension
      });

      // Download the data with jquery to avoid recline to re-download the
      // dataset every time a dataset parameter is changed.
      $.ajax({
        type: 'GET',
        url: url,
        xhr: function() {
          var xhr = new window.XMLHttpRequest();
          xhr.onprogress = downloadProgressCallback;
          return xhr;
        },
        success: function(data) {
          dataset.set('data', data);
          dataset.fetch()
          .done(function(dataset) {
            dfd.resolve(dataset);
          });
        },
        error: function(jqXHR) {
          dfd.reject(jqXHR.status + ': ' + jqXHR.statusText + ' - ' + jqXHR.responseText);
        }
      });

      return dfd.promise();
    },

    // parse XLS
    parseXLSWorkbook: function(bstr, promise) {
      try {
        var workbook = XLSX.read(bstr, {type:"binary"});
        // Show overlay
        $('#visualization-tool').prepend(
          '<div id="sheet-names-form" class="form-group"></div>');
        $('#sheet-names-form').append(
          '<label for="sheet_names">' + vt_intl.t('Sheet Names:') + '</label>' +
          '<select class="form-control" id="sheet_names"></select>'
        );
        snippetHelper.appendToList(workbook.SheetNames);

        // Select first sheet
        $('#sheet_names').val(workbook.SheetNames[0]);
        var sheetData = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
        var dataSet = new recline.Model.Dataset({
          records: sheetData,
        });
        promise.resolve(dataSet);

        // Sheet name selection Listener
        $('#sheet_names').change(function() {
          // Get Sheet data in csv
          var sheetName = $(this).children('option:selected').val();
          if (sheetName == vt_intl.t('Select a sheet')) { return; }

          var sheetData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);

          // Get Dataset
          var dataSet = new recline.Model.Dataset({
            records: sheetData,
          });

          if (window.multiView) { window.multiView.remove(); }
          window.multiView = snippetHelper.createMultiView(dataSet);
        });

      }
      catch(err) {
        promise.reject(vt_intl.t('Workbook Parse Error') + ': ' + err.message);
      }
    },

    // generate XLS
    generateXLSDataSet: function(url) {
      var dfd = jQuery.Deferred();

      /* set up XMLHttpRequest */
      var oReq = new XMLHttpRequest();
      oReq.open("GET", url + '/stream', true);
      oReq.responseType = "arraybuffer";
      oReq.onerror = function(evt) {
        dfd.reject(oReq.status + ': ' + oReq.statusText);
      };
      oReq.onprogress = downloadProgressCallback;
      oReq.onload = function(evt) {
        // Reject and Return if call unsuccessful
        if (oReq.status != 200 && oReq.status != 304) {
          dfd.reject(oReq.status + ': ' + oReq.statusText);
          return;
        }

        // Progress Bar Update
        $('#progress-bar').width('100%');
        $('#progress-bar').text(vt_intl.t('Parsing Excel Workbook'));

        // convert data to binary string
        var arraybuffer = oReq.response;
        var data = new Uint8Array(arraybuffer);
        var arr = [];
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");

        // Give a chance for jquery to refresh the progress bar
        // before parsing the workbook
        window.setTimeout(snippetHelper.parseXLSWorkbook, 200, bstr, dfd);
      };

      oReq.send();
      return dfd.promise();
    },

    // generate dataset
    generateDataSet: function(url, extension) {
      var dfd = jQuery.Deferred();

      // Retrieve appropriate dataset function
      var dataGenerationFunction;
      switch(extension) {
        case 'xls':
        case 'xlsx':
          dataGenerationFunction = snippetHelper.generateXLSDataSet;
          break;
        case 'csv':
        case 'xml':
          dataGenerationFunction = snippetHelper.generateCSVDataSet;
          break;
      }

      // Generate DataSet
      dataGenerationFunction(url, extension)
      .then(
        function(dataSet) {
          dfd.resolve(dataSet);
        },
        function(errorMessage) {
          dfd.reject(errorMessage);
        }
      );

      return dfd.promise();
    },

    // gathers the report link and fills form
    // displays error message
    displayError: function(errorMessage, url) {
      var errorDiv =
          '<div class="alert alert-danger" >'
            + '<h1>'
                + '<i class="fa fa-fw fa-times-circle"></i>' + vt_intl.t('Visualisation Error')
            + '</h1>'
            + '<h3><strong>' + errorMessage + ". " + '</strong>' + '</h3>'
            + '<p>' + vt_intl.t('Download file') + ' <a href="' + url + '">' + vt_intl.t('Download file link') + '</a></p>'
        + '</div>';

      $('#feedback-message').empty();
      $('#feedback-message').append(errorDiv);
    },

    // gathers the report link and fills form
    // displays error message
    displayErrorWithLink: function(errorMessage, url, link) {
      var errorDiv =
          '<div class="alert alert-danger" >'
            + '<h1>'
                + '<i class="fa fa-fw fa-times-circle"></i>' + vt_intl.t('Visualisation Error')
            + '</h1>'
            + '<h3><strong>' + errorMessage + ". " + '</strong>' + link + '</h3>'
            + '<p>' + vt_intl.t('Download file') + ' <a href="' + url + '">' + vt_intl.t('Download file link') + '</a></p>'
        + '</div>';

      $('#feedback-message').empty();
      $('#feedback-message').append(errorDiv);
    },

    // fill the appropriated fields and generates the field to the JIRA form
    reportToJira: function (summary, description, dataset, component, errorType) {
      // set the respective language for the errors
      // translation to match it
      var lang = vt_intl.getLanguage();
      var link = 'http://www.europeandataportal.eu/'
        + lang + '/feedback/form?'
        + 'type=0' // 0 For Bug
        + '&summary=' + summary // Summary of the problem
        + '&description=' + description // Description of the problem
        + '&dataset=' + dataset // DataSet name??
        + '&component=' + component // Component visualisation
        + '&errortype=' + errorType // 404 : Error number

    var a = '<a id="jira" href="' + link + '" target="_blank">'+vt_intl.t('Report Error')+'.</a>';
    return a;
    },

    // guess file extension
    guessFileExtenstion: function(fileNameExtension, contentType) {
      var extension = '';

      // Get Content Type Extension
      var contentExtension = '';
      if (!!contentType) {
        if (contentType == 'application/vnd.ms-excel') {
          contentExtension = 'xls';
        } else if (contentType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
          contentExtension = 'xlsx';
        } else if (contentType.indexOf('text/csv') > -1 || contentType.indexOf('application/octet-stream') > -1) {
          contentExtension = 'csv';
        }
      }

      // Use the content type extension if no file extension
      if (!fileNameExtension) {
        extension = contentExtension;
      } else {
        extension = fileNameExtension;
      }

      return extension;
    },

    // check if the link is correct or not
    urlExists: function urlExists(url, callback) {
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          callback(xhr.status < 400);
        }
      };
      xhr.open('HEAD', url);
      xhr.send();
    },

    init: function() {
        var lang = vt_intl.getLanguage()
        vt_intl.setLanguage(lang);
        // snippetHelper.checkContentDisposition();
        $('.file-preview').click(function(event) {
            event.preventDefault();
            // Get file extension from url
            var url = this.getAttribute('data-file');
            var officeExt = ['doc', 'docx', 'docm', 'ppt', 'pptx', 'pptm'].join('|');
            var extRegex = new RegExp('(?:\\.|format=)(xlsx*|csv|pdf|xml|' + officeExt + ')') ;
            var fileNameExtension = _.last(url.toLowerCase().match(extRegex));
            var encodedURL = NODE_ENV + '/files/' + encodeURIComponent(url);

            // Show MultiView Modal
            overlay.toggleOverlay(true);
            $('#sheet-names-form').remove();
            window.explorerDiv = $('.data-explorer');
            $('#feedback-message').append(
              '<h1><i class="fa fa-fw fa-spinner fa-pulse"></i> '+ vt_intl.t('Loading Visualisation') +' </h1>' +
              '<div class="progress" id="progress-bar-wrapper">' +
                '<div id="progress-bar" class="progress-bar progress-bar-striped active" role="progressbar" style="width: 0%; transition: width .2s ease;">' +
                '</div>' +
              '</div>'
            );

            // Get Url Headers to determine extension and size of the file
            $.get(encodedURL)

            .done(function(data, textStatus, jqXHR) {

                // !!data['Content-Length] = false if 0, null or undefined, else true
                var fileSize = !!data['content-Length'] ? data['Content-Length'] : 0;

                // Guess File type from header
                var extension = snippetHelper.guessFileExtenstion(fileNameExtension, data['content-type']);

                // Check if we can preview the file
                var fileTypeIsSupported = extension == 'xls' || extension == 'xlsx' || extension == 'csv' || extension == 'xml';
                var fileSizeIsUnderLimit = fileSize < 50000000;

                if (fileTypeIsSupported && fileSizeIsUnderLimit) {
                    snippetHelper.generateDataSet(this.url + '/stream', extension)
                    .then(
                      function(dataSet) {
                        // Create Multiview
                        if (window.multiView) { window.multiView.remove(); }
                        window.multiView = snippetHelper.createMultiView(dataSet);
                        // Remove feedback message
                        $('#feedback-message').empty();
                      },
                      function(errorMessage) {

                        // HEAD var link = snippetHelper.reportToJira(vt_intl.t('Visualisation Error'), vt_intl.t(errorMessage), url, 'Component: vistualisation tool', jqXHR.status);
                        // snippetHelper.reportToJira(summary, description, dataset, component, errortype)
                        var link = snippetHelper.reportToJira(vt_intl.t('Visualisation Error'), errorMessage + " : " + this.url + '/stream' + extension, url, 'Component: vistualisation tool', jqXHR.status);
                        snippetHelper.displayError(errorMessage, url);
                      }
                    );
                  } else {
                    var errorMessage = '';

                    if (!fileSizeIsUnderLimit) {
                      errorMessage = vt_intl.t('File too big');
                    }
                    if (!fileTypeIsSupported) {
                      errorMessage = vt_intl.t('File Type not supported');
                    }
                    // HEAD var link = snippetHelper.reportToJira(vt_intl.t('Visualisation Error'), vt_intl.t(errorMessage), url, 'Component: vistualisation tool', jqXHR.status);
                    var link = snippetHelper.reportToJira(vt_intl.t('Visualisation Error'), errorMessage + " : " + this.url + '/stream' + extension, url, 'Component: vistualisation tool', jqXHR.status);

                    // Find the most appropriate way to display the file
                    if (_.contains(OFFICE_EXT, extension)) {
                      snippetHelper.displayError(errorMessage, 'https://view.officeapps.live.com/op/view.aspx?src=' + encodeURIComponent(url));
                    } else {
                      snippetHelper.displayError(errorMessage, url);
                    }
                  }
            })
            // fail case
            .fail(function(jqXHR, status) {

                // HEAD var link = snippetHelper.reportToJira(vt_intl.t('Visualisation Error'), jqXHR.status + " : " + jqXHR.responseText, url, 'Component: vistualisation tool', jqXHR.status);
                var link = snippetHelper.reportToJira(vt_intl.t('Visualisation Error'), jqXHR.status + " : " + jqXHR.responseText + " : " + this.url /* + '/stream' + extension*/ , url, 'Component: vistualisation tool', jqXHR.status);
                snippetHelper.displayError(jqXHR.status + ': ' + jqXHR.statusText + ' - ' + jqXHR.responseText, url);
            });
          });
    }
  };

  $('body').append('<div id="vizual-tool-modal" class="overlay">'+ VT_OVERLAY + '</div>');

  snippetHelper.init();
  if (typeof mocha !== 'undefined') {
    mocha.setup({globals: ['explorerDiv']});
    mocha.run();
  }
  vt_intl.setLanguage();
});

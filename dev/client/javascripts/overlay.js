define(['jquery'], function() {
  var MODAL = 'vizual-tool-modal';
  var modalID = '#' + MODAL;

  var hideOverlay = function(event){
    if (event.target.id == MODAL || event.keyCode == 27) {
      $(modalID).removeClass('overlay-open');
      $('body').removeClass('overlay-view');
      $('#feedback-message').empty();
      if (window.multiView) { window.multiView.remove(); }
    }
  };
  $(document).off('keyup', hideOverlay).on('keyup', hideOverlay);
  $('body').off('click', hideOverlay).on('click', hideOverlay);

  var toggleOverlay = function(state) {
    $(modalID).toggleClass('overlay-open', state);
    $('body').toggleClass('overlay-view', state);
  };

  return {
    toggleOverlay: toggleOverlay
  };

});

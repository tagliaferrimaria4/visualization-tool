(function() {
  var getUrlEnv = function(currentUrl) {
    try {
      return document.getElementById('vt_initializor').getAttribute('src').split('/').slice(0,-2).join('/');
    }catch(e){return currentUrl;}
  };

  var getNodeEnv = function(urlEnv){
    try {
      return url = document.getElementById('vt_initializor').getAttribute('data-preprocessor-url') || urlEnv;
    } catch(e) {return urlEnv;}
  };

  var CURRENT_URL = location.protocol + '//' + location.host;
  URL_ENV = getUrlEnv(CURRENT_URL);
  NODE_ENV = getNodeEnv(URL_ENV);

  function asyncLoad() {
    if (window.require) {
      var req = require.config({baseUrl:URL_ENV+'/javascripts'});
      req(['main']);
    } else {
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = URL_ENV+'/javascripts/require.js';
      s.setAttribute('data-main', URL_ENV+'/javascripts/main');
      var x = document.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    }
  }
  if (window.attachEvent) {
    window.attachEvent('onload', asyncLoad);
  } else {
    window.addEventListener('load', asyncLoad, false);
  }

})();

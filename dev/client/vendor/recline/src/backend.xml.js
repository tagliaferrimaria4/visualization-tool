var recline = recline || {};
recline.Backend = recline.Backend || {};
recline.Backend.xml = recline.Backend.xml || {};

// note module is *defined* in qunit tests :-(
if (typeof module !== 'undefined' && module != null && typeof require !== 'undefined') {
  var _ = require('underscore');
  module.exports = recline;
}

(function(my) {
  my.__type__ = 'xml';

  var Deferred = (typeof jQuery !== "undefined" && jQuery.Deferred) || _.Deferred;

  // Fetch data from a Google Docs spreadsheet.
  //
  // For details of config options and returned values see the README in
  // the repo at https://github.com/Recline/backend.gdocs/
  my.fetch = function(dataset) {
    var url = dataset.url;
    var dfd  = new Deferred(); 
   

    var dfd = new Deferred();

  
    // get the actual worksheet data
    //jQuery.get(url, function(d) {
    jQuery.ajax({
      type: "GET",
      url: url,
      dataType: "xml",
      success: function(d) {
        var result = my.parseData(d);
        var fields = _.map(result.fields, function(fieldId) {
          return {id: fieldId};
        }); // ex : fields = [{id:'toto'}, {id:'tata'}] 

        var metadata = _.extend(url, {
              title: "test XML file",
              spreadsheetTitle: "",
              worksheetTitle  : ""
        });
        dfd.resolve({
          metadata: metadata,
          records       : result.records,
          fields        : fields,
          useMemoryStore: true
        });
      }
    });
   

    return dfd.promise();
  };

  // ## parseData
  //
  // Parse data from Google Docs API into a reasonable form
  //
  // :return: tabular data object (hash with keys: field and data).
  // 
  // Issues: seems google docs return columns in rows in random order and not even sure whether consistent across rows.
  my.parseData = function(xml) {

//    var doc = $($.parseXML(xml));
    var doc = $(xml);
    var res = doc.children().children(); //find list of records inside the XML

    var records = [];
    var fields = [];

    //get field names from first record
    $.each($(res[0]).children(), function (i, f) {
        var fieldName = $(f).prop('tagName');
        fields.push(fieldName);
        $('#result').append($('<el>' + fieldName + '</el>'));
    });

    //create array of values for each record
    $.each(res, function (i, r) {
        var record = [];

        $.each($(r).children(), function (i,f) {
            record.push($(f).text());
        });

        records.push(record);

    });

    return {
      fields: fields ,
      records: records
    };
  };


}(recline.Backend.xml));


var express = require('express');
var request = require('request');

var fileRouter = express.Router();

// Stream file
fileRouter.use('/:url/stream', function(req, res) {
  var url = req.params.url;
  req.pipe(request(url)).pipe(res);
});

// Get File headers
fileRouter.use('/:url', function(req, res) {
  var url = req.params.url;

  request({ method: 'HEAD', url: url }, function(error, response, body) {
    var responseData = {};

    if (!error && response.statusCode == 200) {
      responseData = JSON.stringify(response.headers);
    } else if (response.statusCode === 405) {
        responseData = '{"content-length":"405","content-type":"application/vnd.ms-excel"}';
    } else if (response) {
      res.status(response.statusCode);
      responseData = 'The file could not be downloaded.';
    } else {
      res.status(404);
      responseData = 'The file could not be downloaded.';
    }

    res.setHeader('Content-Type', 'text/json');
    res.send(responseData);
  });
});


var exports = module.exports = {};
exports.fileRouter = fileRouter;

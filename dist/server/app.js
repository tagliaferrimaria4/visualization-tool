/*require('look').start();*/

// Include the cluster module
var cluster = require('cluster');

// Code to run if we're in the master process
if (cluster.isMaster) {
  // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
      cluster.fork();
    }

    // Restart a crashed cluster
    cluster.on('exit', function(deadWorker, code, signal) {
      // Restart the worker
      var worker = cluster.fork();

      // Log the event
      console.log('Worker ' + deadWorker.process.pid + ' died with code: ' +
        code + ', and signal: ' + signal);
      console.log('worker ' + worker.process.pid + ' born.');
    });

// Code to run if we're in a worker process
} else {
    var express = require('express');
    var app = express();
    var files = require('./routes/files');
    var allowCrossDomain = function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET');
      res.header('Access-Control-Allow-Headers', 'Content-Type');
      next();
    };

    app.use(express.static('public'));
    app.use(allowCrossDomain);
    app.use('/visualization-tool/files', files.fileRouter);

    app.listen(process.env.PORT || 3000);
}
